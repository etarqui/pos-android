package com.sietecumbres.pos;

import java.util.ArrayList;
import android.app.Application;
 
public class Controller extends Application{
     
    private ArrayList<ModelProducts> myProducts = new ArrayList<ModelProducts>();
	private ArrayList<ModelUnits> cartUnits = new ArrayList<ModelUnits>();
	private ArrayList<ModelCones> cones = new ArrayList<ModelCones>();
    private ModelCart myCart = new ModelCart();
 
    public ModelProducts getProducts(int pPosition) {
        return myProducts.get(pPosition);
    }
     
    public void setProducts(ModelProducts Products) {
        myProducts.add(Products);
    }
    
    public ModelUnits getUnits(int pPosition) {
        return cartUnits.get(pPosition);
    }
     
    public void setProducts(ModelUnits Units) {
    	cartUnits.add(Units);
    }
     
    public ModelCart getCart() {
        return myCart;
    }
  
   public int getProductsArraylistSize() {
        return myProducts.size();
    }
   
   public ModelCones getCones(int pPosition) {
       return cones.get(pPosition);
   }
    
   public void setCones(ModelCones Cones) {
       cones.add(Cones);
   }
   
   // Para el aceptar pedidos
   private ArrayList<ModelProductsAccept> myProductsAccept = new ArrayList<ModelProductsAccept>();
   private ArrayList<ModelUnitsAccept> cartUnitsAccept = new ArrayList<ModelUnitsAccept>();
   private ModelCartAccept myCartAccept = new ModelCartAccept();
   
   public ModelProductsAccept getProductsAccept(int pPosition) {
       return myProductsAccept.get(pPosition);
   }
    
   public void setProductsAccept(ModelProductsAccept Products) {
       myProductsAccept.add(Products);
   }
   
   public ModelUnitsAccept getUnitsAccept(int pPosition) {
       return cartUnitsAccept.get(pPosition);
   }
    
   public void setProductsAccept(ModelUnitsAccept Units) {
   	cartUnitsAccept.add(Units);
   }
    
   public ModelCartAccept getCartAccept() {
       return myCartAccept;
   }
 
  public int getProductsArraylistSizeAccept() {
       return myProductsAccept.size();
   }
    
}