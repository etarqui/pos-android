package com.sietecumbres.pos;

import org.json.JSONObject;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class Global {
	static private Global instance = null;
	private String development = "http://pos.herokuapp.com";
	//private String development = "http://192.168.0.28:3008";
	private String id80 = "";
	private Integer price80 = 0;
	private String name80 = "";
	private String url80 = "";
	private Integer units80 = 0;
	private Integer weight80 = 0;
	private Integer sellPrice80 = 0;
	private String thumb80 = "";
	private String large80 = "";
	private String basePrice80 = "";
	private String id120 = "";
	private Integer price120 = 0;
	private String name120 = "";
	private String url120 = "";
	private Integer units120 = 0;
	private Integer weight120 = 0;
	private Integer sellPrice120 = 0;
	private String thumb120 = "";
	private String large120 = "";
	private String basePrice120 = "";
	private String id200 = "";
	private Integer price200 = 0;
	private String name200 = "";
	private String url200 = "";
	private Integer units200 = 0;
	private Integer weight200 = 0;
	private Integer sellPrice200 = 0;
	private String thumb200 = "";
	private String large200 = "";
	private String basePrice200 = "";
	private Integer aditional = 0;
	private String aditionalBase = "";
	private Integer totalSell = 0;
	private Integer totalWeight = 0;
	private Integer currentUnits = 0;
	private Integer currentWeight = 0;
	private Integer currentSell = 0;
	private Integer currentCone = 0;
	private String token = "";
	private Integer sellId = 1;
	private String currentRequest = "";
	private Integer min80 = 0;
	private Integer min120 = 0;
	private Integer min200 = 0;
	private Integer max80 = 0;
	private Integer max120 = 0;
	private Integer max200 = 0;
	private Integer conePrice80 = 0;
	private Integer conePrice120 = 0;
	private Integer conePrice200 = 0;
	
	private Integer sellFromAPI = 0;
	private Integer weightFromAPI = 0;
	
	private Integer sellIdFromAPI = 1;
	private Integer requesIdFromAPI = 1;
	
	private String role = "";
	
	private JSONObject sell = null;
	
	private Integer productsCount = 0;
	private int requestTotal = 0;
	
	private Global(){
		
	}
	
	static public Global getInstance(){
		if(instance == null){
			instance = new Global();
		}
		return instance;
	}

	public String getDevelopment() {
		return development;
	}
	
	public Integer getUnits80() {
		return units80;
	}

	public void setUnits80(Integer units80) {
		this.units80 = units80;
	}

	public Integer getWeight80() {
		return weight80;
	}

	public void setWeight80(Integer weight80) {
		this.weight80 = weight80;
	}

	public Integer getSellPrice80() {
		return sellPrice80;
	}

	public void setSellPrice80(Integer sellPrice80) {
		this.sellPrice80 = sellPrice80;
	}

	public Integer getUnits120() {
		return units120;
	}

	public void setUnits120(Integer units120) {
		this.units120 = units120;
	}

	public Integer getWeight120() {
		return weight120;
	}

	public void setWeight120(Integer weight120) {
		this.weight120 = weight120;
	}

	public Integer getSellPrice120() {
		return sellPrice120;
	}

	public void setSellPrice120(Integer sellPrice120) {
		this.sellPrice120 = sellPrice120;
	}

	public Integer getUnits200() {
		return units200;
	}

	public void setUnits200(Integer units200) {
		this.units200 = units200;
	}

	public Integer getWeight200() {
		return weight200;
	}

	public void setWeight200(Integer weight200) {
		this.weight200 = weight200;
	}

	public Integer getSellPrice200() {
		return sellPrice200;
	}

	public void setSellPrice200(Integer sellPrice200) {
		this.sellPrice200 = sellPrice200;
	}
	
	public boolean hasConnection(Context context) {		
		boolean connected = false;
	    ConnectivityManager connectivitymanager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo networkinfo = connectivitymanager.getActiveNetworkInfo();
	    connected = ((networkinfo != null) && (networkinfo.isAvailable()) && (networkinfo
	            .isConnected()));
	    Log.v("Message ", connected + "");
	    if (connected == false) {
	    	Log.v("not connected ", connected + "");
		    return false;
	    } else {
	    	Log.v("connected ", connected + "");
		    return true;
	    }
	}

	public Integer getPrice80() {
		return price80;
	}

	public void setPrice80(Integer price80) {
		this.price80 = price80;
	}

	public Integer getPrice120() {
		return price120;
	}

	public void setPrice120(Integer price120) {
		this.price120 = price120;
	}

	public Integer getAditional() {
		return aditional;
	}

	public void setAditional(Integer aditional) {
		this.aditional = aditional;
	}

	public Integer getPrice200() {
		return price200;
	}

	public void setPrice200(Integer price200) {
		this.price200 = price200;
	}

	public JSONObject getSell() {
		return sell;
	}

	public void setSell(JSONObject sell) {
		this.sell = sell;
	}

	public String getName80() {
		return name80;
	}

	public void setName80(String name80) {
		this.name80 = name80;
	}

	public String getName120() {
		return name120;
	}

	public void setName120(String name120) {
		this.name120 = name120;
	}

	public String getName200() {
		return name200;
	}

	public void setName200(String name200) {
		this.name200 = name200;
	}

	public String getUrl80() {
		return url80;
	}

	public void setUrl80(String url80) {
		this.url80 = url80;
	}

	public String getUrl120() {
		return url120;
	}

	public void setUrl120(String url120) {
		this.url120 = url120;
	}

	public String getUrl200() {
		return url200;
	}

	public void setUrl200(String ur200) {
		this.url200 = ur200;
	}

	public Integer getTotalSell() {
		return totalSell;
	}

	public void setTotalSell(Integer totalSell) {
		this.totalSell = totalSell;
	}

	public Integer getCurrentUnits() {
		return currentUnits;
	}

	public void setCurrentUnits(Integer currentUnits) {
		this.currentUnits = currentUnits;
	}

	public Integer getCurrentWeight() {
		return currentWeight;
	}

	public void setCurrentWeight(Integer currentWeight) {
		this.currentWeight = currentWeight;
	}

	public Integer getCurrentSell() {
		return currentSell;
	}

	public void setCurrentSell(Integer currentSell) {
		this.currentSell = currentSell;
	}

	public Integer getCurrentCone() {
		return currentCone;
	}

	public void setCurrentCone(Integer currentCone) {
		this.currentCone = currentCone;
	}

	public String getId80() {
		return id80;
	}

	public void setId80(String id80) {
		this.id80 = id80;
	}

	public String getId120() {
		return id120;
	}

	public void setId120(String id120) {
		this.id120 = id120;
	}

	public String getId200() {
		return id200;
	}

	public void setId200(String id200) {
		this.id200 = id200;
	}

	public Integer getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(Integer totalWeight) {
		this.totalWeight = totalWeight;
	}

	public String getThumb80() {
		return thumb80;
	}

	public void setThumb80(String thumb80) {
		this.thumb80 = thumb80;
	}

	public String getThumb120() {
		return thumb120;
	}

	public void setThumb120(String thumb120) {
		this.thumb120 = thumb120;
	}

	public String getThumb200() {
		return thumb200;
	}

	public void setThumb200(String thumb200) {
		this.thumb200 = thumb200;
	}

	public String getLarge80() {
		return large80;
	}

	public void setLarge80(String large80) {
		this.large80 = large80;
	}

	public String getLarge120() {
		return large120;
	}

	public void setLarge120(String large120) {
		this.large120 = large120;
	}

	public String getLarge200() {
		return large200;
	}

	public void setLarge200(String large200) {
		this.large200 = large200;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Integer getSellId() {
		return sellId;
	}

	public void setSellId(Integer sellId) {
		this.sellId = sellId;
	}

	public String getAditionalBase() {
		return aditionalBase;
	}

	public void setAditionalBase(String aditionalBase) {
		this.aditionalBase = aditionalBase;
	}

	public String getBasePrice200() {
		return basePrice200;
	}

	public void setBasePrice200(String basePrice200) {
		this.basePrice200 = basePrice200;
	}

	public String getBasePrice120() {
		return basePrice120;
	}

	public void setBasePrice120(String basePrice120) {
		this.basePrice120 = basePrice120;
	}

	public String getBasePrice80() {
		return basePrice80;
	}

	public void setBasePrice80(String basePrice80) {
		this.basePrice80 = basePrice80;
	}

	public Integer getSellFromAPI() {
		return sellFromAPI;
	}

	public void setSellFromAPI(Integer sellFromAPI) {
		this.sellFromAPI = sellFromAPI;
	}

	public Integer getWeightFromAPI() {
		return weightFromAPI;
	}

	public void setWeightFromAPI(Integer weightFromAPI) {
		this.weightFromAPI = weightFromAPI;
	}

	public Integer getSellIdFromAPI() {
		return sellIdFromAPI;
	}

	public void setSellIdFromAPI(Integer sellIdFromAPI) {
		this.sellIdFromAPI = sellIdFromAPI;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Integer getProductsCount() {
		return productsCount;
	}

	public void setProductsCount(Integer productsCount) {
		this.productsCount = productsCount;
	}

	public Integer getRequesIdFromAPI() {
		return requesIdFromAPI;
	}

	public void setRequesIdFromAPI(Integer requesIdFromAPI) {
		this.requesIdFromAPI = requesIdFromAPI;
	}

	public int getRequestTotal() {
		return requestTotal;
	}

	public void setRequestTotal(int requestTotal) {
		this.requestTotal = requestTotal;
	}

	public String getCurrentRequest() {
		return currentRequest;
	}

	public void setCurrentRequest(String currentRequest) {
		this.currentRequest = currentRequest;
	}

	public Integer getMin80() {
		return min80;
	}

	public void setMin80(Integer min80) {
		this.min80 = min80;
	}

	public Integer getMin120() {
		return min120;
	}

	public void setMin120(Integer min120) {
		this.min120 = min120;
	}

	public Integer getMin200() {
		return min200;
	}

	public void setMin200(Integer min200) {
		this.min200 = min200;
	}

	public Integer getConePrice80() {
		return conePrice80;
	}

	public void setConePrice80(Integer conePrice80) {
		this.conePrice80 = conePrice80;
	}

	public Integer getConePrice120() {
		return conePrice120;
	}

	public void setConePrice120(Integer conePrice120) {
		this.conePrice120 = conePrice120;
	}

	public Integer getConePrice200() {
		return conePrice200;
	}

	public void setConePrice200(Integer conePrice200) {
		this.conePrice200 = conePrice200;
	}

	public Integer getMax80() {
		return max80;
	}

	public void setMax80(Integer max80) {
		this.max80 = max80;
	}

	public Integer getMax120() {
		return max120;
	}

	public void setMax120(Integer max120) {
		this.max120 = max120;
	}

	public Integer getMax200() {
		return max200;
	}

	public void setMax200(Integer max200) {
		this.max200 = max200;
	}
	
}