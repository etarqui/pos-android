package com.sietecumbres.pos;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.speech.tts.SynthesisRequest;
import android.util.Log;
import android.widget.Toast;

public class DatabaseHandler extends SQLiteOpenHelper{
	
	//Ruta por defecto de las bases de datos en el sistema Android
	private static String DB_PATH = "/data/data/com.sietecumbres.pos/databases/";
	 
	private static String DB_NAME = "Pos";
	 
	private SQLiteDatabase myDataBase;
	 
	private final Context myContext;

	// Sells Table Columns names
	private static final String SELL = "sell";
	private static final String KEY_ID = "id";
	private static final String KEY_TOTAL_SELL = "total_sell";
	private static final String KEY_TOTAL_WEIGHT = "total_weight";
	private final ArrayList<Sell> sell_list = new ArrayList<Sell>();
	
	// Sell Items Table Columns names
	private static final String SELL_ITEM = "sell_item";
	private static final String ITEMS_KEY_ID = "id";
	private static final String ITEMS_KEY_PRODUCT = "product";
	private static final String ITEMS_KEY_QUANTITY = "quantity";
	private static final String ITEMS_KEY_WEIGHT = "weight";
	private static final String ITEMS_KEY_VALUE = "value";
	private static final String ITEMS_KEY_SELL = "sell_id";
	private static final String ITEMS_KEY_PRODUCT_N = "product_n";
	private final ArrayList<SellItem> sell_item_list = new ArrayList<SellItem>();
	
	private static final String TOTALS = "totals";
	private static final String TOTALS_KEY_ID = "id";
	private static final String TOTALS_DAILY_SALE = "daily_sale";
	private static final String TOTALS_INVENTORY = "weight";
	
	private static final String PARAMETERS = "parameters";
	private static final String PARAMETERS_KEY_ID = "id";
	private static final String PARAMETERS_VALUE_80 = "value80";
	private static final String PARAMETERS_VALUE_120 = "value120";
	private static final String PARAMETERS_VALUE_200 = "value200";
	private static final String PARAMETERS_NAME_80 = "name80";
	private static final String PARAMETERS_NAME_120 = "name120";
	private static final String PARAMETERS_NAME_200 = "name200";
	private static final String PARAMETERS_ADITIONAL = "aditional";
	private static final String PARAMETERS_ID_80 = "id80";
	private static final String PARAMETERS_ID_120 = "id120";
	private static final String PARAMETERS_ID_200 = "id200";

	// Requests Table Columns names
	private static final String REQUEST = "request";
	private static final String REQUEST_KEY_ID = "id";
	private static final String REQUEST_KEY_TOTAL_SELL = "total_price";
	private final ArrayList<Request> request_list = new ArrayList<Request>();
	
	// Request Items Table Columns names
	private static final String REQUEST_ITEM = "request_item";
	private static final String REQUEST_ITEM_KEY_ID = "id";
	private static final String REQUEST_ITEM_KEY_PRODUCT = "product";
	private static final String REQUEST_ITEM_KEY_QUANTITY = "quantity";
	private static final String REQUEST_ITEM_KEY_VALUE = "value";
	private static final String REQUEST_ITEM_KEY_REQUEST = "request_id";
	private final ArrayList<RequestItem> request_item_list = new ArrayList<RequestItem>();
	
	/**
	* Constructor
	* Toma referencia hacia el contexto de la aplicaci�n que lo invoca para poder acceder a los 'assets' y 'resources' de la aplicaci�n.
	* Crea un objeto DBOpenHelper que nos permitir� controlar la apertura de la base de datos.
	* @param context
	*/
	public DatabaseHandler(Context context) {
		super(context, DB_NAME, null, 1);
		this.myContext = context;
	}
	 
	/**
	* Crea una base de datos vac�a en el sistema y la reescribe con nuestro fichero de base de datos.
	* */
	public void createDataBase() throws IOException{
	 
		boolean dbExist = checkDataBase();
	 
		if(dbExist){
		}else{
			this.getWritableDatabase();
			try {
				copyDataBase();
			} catch (IOException e) {
				throw new Error("Error copiando Base de Datos");
			}
		}
	}
	 
	/**
	* Comprueba si la base de datos existe para evitar copiar siempre el fichero cada vez que se abra la aplicaci�n.
	* @return true si existe, false si no existe
	*/
	private boolean checkDataBase(){

		//myContext.deleteDatabase(DB_NAME);
		
		SQLiteDatabase checkDB = null;
	 
		try{
	 
			String myPath = DB_PATH + DB_NAME;
			checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
	 
		}catch(SQLiteException e){
	 
			//si llegamos aqui es porque la base de datos no existe todav�a.
	 
		}
		if(checkDB != null){
	 
			checkDB.close();
	 
		}
		return checkDB != null ? true : false;
	}
	 
	/**
	* Copia la base de datos desde la carpeta assets a la reci�n creada
	* base de datos en la carpeta de sistema, desde d�nde podremos acceder a ella.
	* Esto se hace con bytestream.
	* */
	private void copyDataBase() throws IOException{
	 
		//Abrimos el fichero de base de datos como entrada
		InputStream myInput = myContext.getAssets().open(DB_NAME);
	 
		//Ruta a la base de datos vac�a reci�n creada
		String outFileName = DB_PATH + DB_NAME;
	 
		//Abrimos la base de datos vac�a como salida
		OutputStream myOutput = new FileOutputStream(outFileName);
	 
		//Transferimos los bytes desde el fichero de entrada al de salida
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer))>0){
			myOutput.write(buffer, 0, length);
		}
	 
		//Liberamos los streams
		myOutput.flush();
		myOutput.close();
		myInput.close();
	 
	}
	 
	public void open() throws SQLException{

		//Abre la base de datos
		try {
			createDataBase();
		} catch (IOException e) {
			throw new Error("Ha sido imposible crear la Base de Datos");
		}
	 
		String myPath = DB_PATH + DB_NAME;
		myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
	 
	}
	 
	@Override
	public synchronized void close() {
		if(myDataBase != null)
			myDataBase.close();
		super.close();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}
	/**
	* A continuaci�n se crear�n los m�todos de lectura, inserci�n, actualizaci�n
	* y borrado de la base de datos.
	* */
	
	/**
	 * Count Sells
	 */
	public Cursor countSells() {
		return myDataBase.rawQuery("SELECT COUNT(*) FROM sell", null);
	}
	
	public int countSellsInt() {
		int count = 0;
		
		Cursor cur = countSells();
		if (cur != null) {
		    cur.moveToFirst();
		    if(cur.getInt(0) > 0){
		    	count = cur.getInt(0);
		    }
		}
		
		return count;
	}
	
	/**
	* Insert Sell
	**/
	public long insertSell(int total_sell, int total_weight) {
		Cursor cur = countSells();
		
		int id = Global.getInstance().getSellIdFromAPI() + 1;
		Global.getInstance().setSellIdFromAPI(id);
		
		long result = 0;
		if (cur != null) {
		    cur.moveToFirst();
		    cur.getInt(0);
		    ContentValues newValues = new ContentValues();
			newValues.put(KEY_ID, (id));
			newValues.put(KEY_TOTAL_SELL, total_sell);
			newValues.put(KEY_TOTAL_WEIGHT, total_weight);
			result = myDataBase.insert(SELL, null, newValues);
		}
		return result;
	}
	
	/**
	 * Get Sells
	 */
	 
	// Getting All Sells
    public ArrayList<Sell> Get_Sells() {
        try {
            sell_list.clear();

            // Select All Query
            String selectQuery = "SELECT  * FROM " + SELL;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Sell sell = new Sell();
                    sell.setID(Integer.parseInt(cursor.getString(0)));
                    sell.setTotalSell(Integer.parseInt(cursor.getString(1)));
                    sell.setTotalWeight(Integer.parseInt(cursor.getString(2)));
                    // Adding contact to list
                    sell_list.add(sell);
                } while (cursor.moveToNext());
            }

            // return contact list
            cursor.close();
            db.close();
            return sell_list;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("all_contact", "" + e);
        }

        return sell_list;
    }
    
    public long removeSell(Integer id){
		return myDataBase.delete("SELL", KEY_ID + "=" + id, null);
	}
    
    public int getLastSellId(){
    	int id = 1;
    	String query = "SELECT * FROM sell WHERE ID = (SELECT MAX(ID) FROM sell)";
    	Cursor cursor = myDataBase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
            	id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
    	return id;
    }
	
	/**
	 * Count Sell Items
	 */
	public Cursor countSellItems() {
		return myDataBase.rawQuery("SELECT COUNT(*) FROM sell_item", null);
	}
	
	/**
	* Insert Sell Item
	**/
	public long insertSellItem(String product, int quantity, int weight, int value, int sell_id, int product_n) {
		Cursor cur = countSellItems();
		long result = 0;
		if (cur != null) {
		    cur.moveToFirst();
		    cur.getInt(0);
		    ContentValues newValues = new ContentValues();
			newValues.put(ITEMS_KEY_ID, (cur.getInt(0) + 1));
			newValues.put(ITEMS_KEY_PRODUCT, product);
			newValues.put(ITEMS_KEY_QUANTITY, quantity);
			newValues.put(ITEMS_KEY_WEIGHT, weight);
			newValues.put(ITEMS_KEY_VALUE, value);
			newValues.put(ITEMS_KEY_SELL, sell_id);
			newValues.put(ITEMS_KEY_PRODUCT_N, product_n);
			result = myDataBase.insert(SELL_ITEM, null, newValues);
		}
		return result;
	}
	
	/**
	 * Get Sell Items
	 */
	 
	// Getting All Contacts
    public ArrayList<SellItem> Get_Sell_Items(int sell_id) {
        try {
            sell_item_list.clear();

            // Select All Query
            String selectQuery = "SELECT  * FROM " + SELL_ITEM + " WHERE " + ITEMS_KEY_SELL + " = " + sell_id;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                	SellItem sell_item = new SellItem();
                	sell_item.setID(Integer.parseInt(cursor.getString(0)));
                	sell_item.setProduct(cursor.getString(1));
                	sell_item.setQuantity(Integer.parseInt(cursor.getString(2)));
                	sell_item.setWeight(Integer.parseInt(cursor.getString(3)));
                	sell_item.setValue(Integer.parseInt(cursor.getString(4)));
                	sell_item.setSellId(Integer.parseInt(cursor.getString(5)));
                	sell_item.setProductN(Integer.parseInt(cursor.getString(6)));
                    // Adding contact to list
                	sell_item_list.add(sell_item);
                } while (cursor.moveToNext());
            }

            // return contact list
            cursor.close();
            db.close();
            return sell_item_list;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("all_contact", "" + e);
        }

        return sell_item_list;
    }
    
    public long removeSellItem(Integer sell_id){
		return myDataBase.delete("SELL_ITEM", ITEMS_KEY_SELL + "=" + sell_id, null);
	}
    
    /*
     * Totales
     */
    
	public Cursor hasTotals() {
		return myDataBase.rawQuery("SELECT COUNT(*) FROM totals", null);
	}
	
	public long updateTotals(int dailySale, int inventory) {
		
		Cursor cur = hasTotals();
		long result = 0;

		ContentValues newValues = new ContentValues();
		newValues.put(TOTALS_DAILY_SALE, dailySale);
		newValues.put(TOTALS_INVENTORY, inventory);
		
		if (cur != null) {
		    cur.moveToFirst();
		    if (cur.getInt (0) == 0) {
				newValues.put(TOTALS_KEY_ID, 1);
				result = myDataBase.insert(TOTALS, null, newValues);
		    } else {
				result = myDataBase.update(TOTALS, newValues, TOTALS_KEY_ID + "=" + 1, null);
		    }
		}
		
		return result;
	}
	
	public Cursor getTotals() {
		return myDataBase.rawQuery("SELECT * FROM totals", null);
	}
    
    /*
     * Parameters
     */
    
	public Cursor hasParameters() {
		return myDataBase.rawQuery("SELECT COUNT(*) FROM parameters", null);
	}
	
	public long updateParameters(int value80, int value120, int value200, String name80, String name120, String name200, int aditional, String id80, String id120, String id200) {
		
		Cursor cur = hasParameters();
		long result = 0;

		ContentValues newValues = new ContentValues();
		newValues.put(PARAMETERS_VALUE_80, value80);
		newValues.put(PARAMETERS_VALUE_120, value120);
		newValues.put(PARAMETERS_VALUE_200, value200);
		newValues.put(PARAMETERS_NAME_80, name80);
		newValues.put(PARAMETERS_NAME_120, name120);
		newValues.put(PARAMETERS_NAME_200, name200);
		newValues.put(PARAMETERS_ADITIONAL, aditional);
		newValues.put(PARAMETERS_ID_80, id80);
		newValues.put(PARAMETERS_ID_120, id120);
		newValues.put(PARAMETERS_ID_200, id200);
		
		if (cur != null) {
		    cur.moveToFirst();
		    if (cur.getInt (0) == 0) {
				newValues.put(PARAMETERS_KEY_ID, 1);
				result = myDataBase.insert(PARAMETERS, null, newValues);
		    } else {
				result = myDataBase.update(PARAMETERS, newValues, PARAMETERS_KEY_ID + "=" + 1, null);
		    }
		}
		
		return result;
	}
	
	public Cursor getParameters() {
		return myDataBase.rawQuery("SELECT * FROM parameters", null);
	}
	
	/**
	 * Count Requests
	 */
	public Cursor countRequests() {
		return myDataBase.rawQuery("SELECT COUNT(*) FROM request", null);
	}
	
	public int countRequestsInt() {
		int count = 0;
		
		Cursor cur = countRequests();
		if (cur != null) {
		    cur.moveToFirst();
		    if(cur.getInt(0) > 0){
		    	count = cur.getInt(0);
		    }
		}
		
		return count;
	}
	
	/**
	* Insert Request
	**/
	public long insertRequest(int total_price) {
		Cursor cur = countRequests();
		
		int id = Global.getInstance().getRequesIdFromAPI() + 1;
		Global.getInstance().setRequesIdFromAPI(id);
		
		long result = 0;
		if (cur != null) {
		    cur.moveToFirst();
		    cur.getInt(0);
		    ContentValues newValues = new ContentValues();
			newValues.put(REQUEST_KEY_ID, (id));
			newValues.put(REQUEST_KEY_TOTAL_SELL, total_price);
			result = myDataBase.insert(REQUEST, null, newValues);
		}
		return result;
	}
	
	/**
	 * Get Requests
	 */
	 
	// Getting All Sells
    public ArrayList<Request> Get_Requests() {
        try {
            request_list.clear();

            // Select All Query
            String selectQuery = "SELECT  * FROM " + REQUEST;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Request request = new Request();
                    request.setID(Integer.parseInt(cursor.getString(0)));
                    request.setTotalPrice(Integer.parseInt(cursor.getString(1)));
                    // Adding contact to list
                    request_list.add(request);
                } while (cursor.moveToNext());
            }

            // return contact list
            cursor.close();
            db.close();
            return request_list;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("all_contact", "" + e);
        }

        return request_list;
    }
    
    public long removeRequest(Integer id){
		return myDataBase.delete("REQUEST", REQUEST_KEY_ID + "=" + id, null);
	}
    
    public int getLastRequestId(){
    	int id = 1;
    	String query = "SELECT * FROM request WHERE ID = (SELECT MAX(ID) FROM request)";
    	Cursor cursor = myDataBase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
            	id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
    	return id;
    }
	
	/**
	 * Count Request Items
	 */
	public Cursor countRequestItems() {
		return myDataBase.rawQuery("SELECT COUNT(*) FROM request_item", null);
	}
	
	/**
	* Insert Request Item
	**/
	public long insertRequestItem(String product, int quantity, int value, int request_id) {
		Cursor cur = countRequestItems();
		long result = 0;
		if (cur != null) {
		    cur.moveToFirst();
		    cur.getInt(0);
		    ContentValues newValues = new ContentValues();
			newValues.put(REQUEST_ITEM_KEY_ID, (cur.getInt(0) + 1));
			newValues.put(REQUEST_ITEM_KEY_PRODUCT, product);
			newValues.put(REQUEST_ITEM_KEY_QUANTITY, quantity);
			newValues.put(REQUEST_ITEM_KEY_VALUE, value);
			newValues.put(REQUEST_ITEM_KEY_REQUEST, request_id);
			result = myDataBase.insert(REQUEST_ITEM, null, newValues);
		}
		return result;
	}
	
	/**
	 * Get Sell Items
	 */
	 
	// Getting All Contacts
    public ArrayList<RequestItem> Get_Request_Items(int request_id) {
        try {
            request_item_list.clear();

            // Select All Query
            String selectQuery = "SELECT  * FROM " + REQUEST_ITEM + " WHERE " + REQUEST_ITEM_KEY_REQUEST + " = " + request_id;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                	RequestItem request_item = new RequestItem();
                	request_item.setID(Integer.parseInt(cursor.getString(0)));
                	request_item.setProduct(cursor.getString(1));
                	request_item.setQuantity(Integer.parseInt(cursor.getString(2)));
                	request_item.setValue(Integer.parseInt(cursor.getString(3)));
                	request_item.setRequestId(Integer.parseInt(cursor.getString(4)));
                    // Adding contact to list
                	request_item_list.add(request_item);
                } while (cursor.moveToNext());
            }

            // return contact list
            cursor.close();
            db.close();
            return request_item_list;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("all_contact", "" + e);
        }

        return request_item_list;
    }
    
    public long removeRequestItem(Integer request_id){
		return myDataBase.delete("REQUEST_ITEM", REQUEST_ITEM_KEY_REQUEST + "=" + request_id, null);
	}
	
}