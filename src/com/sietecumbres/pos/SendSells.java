package com.sietecumbres.pos;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.savagelook.android.UrlJsonAsyncTask;

public class SendSells {

	private static DatabaseHandler BD;
	private final static String SELL_URL = Global.getInstance().getDevelopment() + "/api/v1/sell.json";
	public Context context;
	
	public void sendSells(Context c) {
		
		context = c;
		
		BD = new DatabaseHandler(context);
		BD.open();

		ArrayList<Sell> sells_array = BD.Get_Sells();
		
        for (int i = 0; i < sells_array.size(); i++) {

            int tidno = sells_array.get(i).getID();
            int total_sell = sells_array.get(i).getTotalSell();
            int total_weight = sells_array.get(i).getTotalWeight();
            
            Global.getInstance().setTotalSell(total_sell);
            Global.getInstance().setTotalWeight(total_weight);
            
            Toast.makeText(context, tidno + " " + total_sell + " " + total_weight, Toast.LENGTH_LONG).show();

    		ArrayList<SellItem> sell_items_array = BD.Get_Sell_Items(tidno);
    		
            for (int j = 0; j < sell_items_array.size(); j++) {

                String product = sell_items_array.get(i).getProduct();
                int quantity = sell_items_array.get(i).getQuantity();
                int weight = sell_items_array.get(i).getWeight();
                int value = sell_items_array.get(i).getValue();
                int product_n = sell_items_array.get(i).getProductN();
                
                Toast.makeText(context, "Dentro del for de items" + product_n + " " + quantity + " " + weight, Toast.LENGTH_LONG).show();
                
                if(product_n == 80){
                	Global.getInstance().setUnits80(quantity);
                	Global.getInstance().setWeight80(weight);
                	Global.getInstance().setSellPrice80(value);
                	Global.getInstance().setId80(product);
                }
                if(product_n == 120){
                	Global.getInstance().setUnits120(quantity);
                	Global.getInstance().setWeight120(weight);
                	Global.getInstance().setSellPrice120(value);
                	Global.getInstance().setId120(product);
                }
                if(product_n == 200){
                	Global.getInstance().setUnits200(quantity);
                	Global.getInstance().setWeight200(weight);
                	Global.getInstance().setSellPrice200(value);
                	Global.getInstance().setId200(product);
                }
                acceptSell();
            }
        }
        BD.close();
	}
	
	public void acceptSell() {
		if(Global.getInstance().hasConnection(context)){
			SellTask sellTask = new SellTask(context);
			sellTask.setMessageLoading("Enviando...");
			sellTask.execute(SELL_URL + "?authentication_token=" + Global.getInstance().getToken());
    	}else{
    		Toast.makeText(context, "El dispositivo no tiene conexi�n a Internet, la venta se encuentra almacenada en la Base de Datos y ser� enviada cuando el dispositivo tenga conexi�n a Internet.", Toast.LENGTH_LONG).show();
    	}
	}

	
	private class SellTask extends UrlJsonAsyncTask {
	    public SellTask(Context context) {
	        super(context);
	    }

	    @Override
	    protected JSONObject doInBackground(String... urls) {
	        DefaultHttpClient client = new DefaultHttpClient();
	        HttpPost post = new HttpPost(urls[0]);
	        JSONObject holder = new JSONObject();
	        JSONObject cone80 = new JSONObject();
	        JSONObject cone120 = new JSONObject();
	        JSONObject cone200 = new JSONObject();
	        JSONObject total = new JSONObject();
	        String response = null;
	        JSONObject json = new JSONObject();

	        try {
	            try {
	                json.put("success", false);
	                json.put("info", "El dispositivo no tiene conexi�n a Internet.");
	                // 80
	                if(Global.getInstance().getSellPrice80() > 0){
	                	cone80.put("id", Global.getInstance().getId80());
	                	cone80.put("units", Global.getInstance().getUnits80());
	                	cone80.put("weight", Global.getInstance().getWeight80());
	                	cone80.put("sell", Global.getInstance().getSellPrice80());
	                	holder.put("cone80", cone80);
	                }
	                // 120
	                if(Global.getInstance().getSellPrice120() > 0){
	                	cone120.put("id", Global.getInstance().getId120());
	                	cone120.put("units", Global.getInstance().getUnits120());
	                	cone120.put("weight", Global.getInstance().getWeight120());
	                	cone120.put("sell", Global.getInstance().getSellPrice120());
	                	holder.put("cone120", cone120);
	                }
	                // 200
	                if(Global.getInstance().getSellPrice200() > 0){
	                	cone200.put("id", Global.getInstance().getId200());
	                	cone200.put("units", Global.getInstance().getUnits200());
	                	cone200.put("weight", Global.getInstance().getWeight200());
	                	cone200.put("sell", Global.getInstance().getSellPrice200());
	                	holder.put("cone200", cone200);
	                }
	                // total
	                total.put("sell", Global.getInstance().getTotalSell());
	                total.put("weight", Global.getInstance().getTotalWeight());
	                holder.put("total", total);
	                
	                StringEntity se = new StringEntity(holder.toString());
	                post.setEntity(se);
	                
	                // setup the request headers
	                post.setHeader("Accept", "application/json");
	                post.setHeader("Content-Type", "application/json");

	                ResponseHandler<String> responseHandler = new BasicResponseHandler();
	                
	        		if(Global.getInstance().hasConnection(context))
	        			response = client.execute(post, responseHandler);
	        		else
	        			response = null;
	        		
	                json = new JSONObject(response);

	            } catch (HttpResponseException e) {
	                e.printStackTrace();
	                Log.e("ClientProtocol", "" + e);
	                json.put("info", "Email o Contrase�a incorrectos.");
	            } catch (IOException e) {
	                e.printStackTrace();
	                Log.e("IO", "" + e);
	            }
	        } catch (JSONException e) {
	            e.printStackTrace();
	            Log.e("JSON", "" + e);
	        }

	        return json;
	    }

	    @Override
	    protected void onPostExecute(JSONObject json) {
	        try {
	            if (json.getBoolean("success")) {
	            	resetGlobals();
	            }
	            Toast.makeText(context, json.getString("info"), Toast.LENGTH_LONG).show();
	        } catch (Exception e) {
	            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
	        } finally {
	            super.onPostExecute(json);
	        }
	    }
	}
	
	public void resetGlobals() {
		Global.getInstance().setUnits80(0);
		Global.getInstance().setUnits120(0);
		Global.getInstance().setUnits200(0);
		Global.getInstance().setWeight80(0);
		Global.getInstance().setWeight120(0);
		Global.getInstance().setWeight200(0);
		Global.getInstance().setSellPrice80(0);
		Global.getInstance().setSellPrice120(0);
		Global.getInstance().setSellPrice200(0);
		Global.getInstance().setTotalSell(0);
		Global.getInstance().setTotalWeight(0);
	}

}
