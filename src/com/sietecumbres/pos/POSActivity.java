package com.sietecumbres.pos;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.savagelook.android.UrlJsonAsyncTask;

public class POSActivity extends SherlockActivity {
	
	private static final String CONES_URL = Global.getInstance().getDevelopment() + "/api/v1/cones.json";
	private final static String SELL_URL = Global.getInstance().getDevelopment() + "/api/v1/sell.json";
	private SharedPreferences mPreferences;

    MemoryCache memoryCache=new MemoryCache();
    
	TextView cone1, cone2, cone3, coneName;
	TextView price1, price2, price3;
	TextView aditional, inventory, cancelTextDialog, dailySales, inventaryDesc, alertTextDialog;
	ImageButton cone80Image, cone120Image, cone200Image;
	ImageView cone;
	
	EditText units, weight;
	TextView price;
	
	Button currentSellButton, historicalSales, requests, currentRequestButton;
	
	Integer sellPrice, totalSell, sellUnits, totalWeight = 0;
	
	DatabaseHandler dbHandler = new DatabaseHandler(this);
	
	Integer status = 0, sellFromBD = 1;

	String id80 = null, id120 = null, id200 = null;
	
	Controller aController = null;
	ModelCones coneObject = null;
	ModelCones coneObject80 = null;
	ModelCones coneObject120 = null;
	ModelCones coneObject200 = null;
	
	LinearLayout inventaryLayout;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pos);
		
		// Change fonts
		Typeface tf = Typeface.createFromAsset(getAssets(),
                "fonts/HelveticaNeueDeskUI.ttf");
		
		mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
		Global.getInstance().setToken(mPreferences.getString("AuthToken", ""));
	
		//Get Global Controller Class object (see application tag in AndroidManifest.xml)
        aController = (Controller) getApplicationContext();

		currentSellButton = (Button)findViewById(R.id.currentSell);
		currentSellButton.setTypeface(tf);
        if(Global.getInstance().getTotalSell() == 0)
        	currentSellButton.setVisibility(View.GONE);

		currentRequestButton = (Button)findViewById(R.id.currentRequest);
		currentRequestButton.setTypeface(tf);
		currentRequestButton.setEnabled(false);
		currentRequestButton.setAlpha(0);
		
		dbHandler.open();
		
		Cursor cur = dbHandler.countSells();
		if (cur != null) {
		    cur.moveToFirst();
		    if(cur.getInt(0) > 0 && (NetworkUtil.getConnectivityStatus(this) == 1 || NetworkUtil.getConnectivityStatus(this) == 2)){
		    	sendSells();
		    }
		}
		
		historicalSales = (Button)findViewById(R.id.historicalSales);
		historicalSales.setVisibility(View.GONE);
		requests = (Button)findViewById(R.id.requests);
		requests.setVisibility(View.GONE);
		
		historicalSales.setTypeface(tf);
		requests.setTypeface(tf);
		
		if(mPreferences.getString("Role", "").equals("admin")){
			historicalSales.setVisibility(View.VISIBLE);
			requests.setVisibility(View.VISIBLE);
		}
		
	}
	
	@Override
	public void onResume() {
	    super.onResume();

	    if (mPreferences.contains("AuthToken")) {
	    	showCones(CONES_URL);
	    } else {
	        Intent intent = new Intent(POSActivity.this, WelcomeActivity.class);
	        startActivityForResult(intent, 0);
	    }
	}
	
	private void showCones(String url) {
		if(Global.getInstance().hasConnection(POSActivity.this)){
			GetCones getCones = new GetCones(POSActivity.this);
			getCones.setMessageLoading("Cargando...");
			getCones.execute(url + "?authentication_token=" + mPreferences.getString("AuthToken", ""));
		}else{
			setConesValuesOffLine();
			Toast.makeText(POSActivity.this, "El dispositivo no tiene conexi�n a Internet.", Toast.LENGTH_LONG).show();
		}
	}
	
	private class GetCones extends UrlJsonAsyncTask {
	    public GetCones(Context context) {
	        super(context);
	    }

		@Override
        protected void onPostExecute(JSONObject json) {
            try {
                JSONArray jsonCones = json.getJSONArray("cones");
                JSONArray jsonAditional = json.getJSONArray("aditional");
                JSONArray jsonInventary = json.getJSONArray("inventory");
                JSONArray jsonSales = json.getJSONArray("sales");
                JSONArray jsonRequests = json.getJSONArray("requests");
                JSONArray jsonLimit = json.getJSONArray("limit");
                JSONArray jsonCurrentRequest = json.getJSONArray("request");
                
                int size = jsonCones.length();

        		Typeface tf = Typeface.createFromAsset(getAssets(),
                        "fonts/HelveticaNeueDeskUI.ttf");
                
                TableLayout conesLayout = (TableLayout) findViewById(R.id.conesTable);
                
                for(int n = 0, s = conesLayout.getChildCount(); n < s; ++n) {
                	TableRow row = (TableRow)conesLayout.getChildAt(n);
                	row.setVisibility(View.GONE);
                }
                
            	TableRow imagesRow = (TableRow) getLayoutInflater().inflate(R.layout.cone_buttons_row, null);
                
                for(int i = 0; i < size; ++i){
                	coneObject = new ModelCones(
                			jsonCones.getJSONObject(i).getString("id"),
                			jsonCones.getJSONObject(i).getString("name"),
                			jsonCones.getJSONObject(i).getString("image"),
                			jsonCones.getJSONObject(i).getString("fixed_price"),
                			jsonCones.getJSONObject(i).getInt("price"),
                			jsonCones.getJSONObject(i).getInt("min_weight"),
                			jsonCones.getJSONObject(i).getInt("cone_price"),
                			jsonCones.getJSONObject(i).getInt("max_weight")
                    );

                    aController.setCones(coneObject);
                    
                	Button ibImages = (Button)imagesRow.getChildAt(i);
                	ibImages.setText(jsonCones.getJSONObject(i).getString("fixed_price"));
            		// Change font type
            		ibImages.setTypeface(tf);
                }
        		
        		conesLayout.addView(imagesRow);
        		conesLayout.requestLayout();
                
                aditional = (TextView)findViewById(R.id.aditional);
                aditional.setText(jsonAditional.getJSONObject(0).getString("fixed_price"));
                Global.getInstance().setAditional(jsonAditional.getJSONObject(0).getInt("price"));
                Global.getInstance().setAditionalBase(jsonAditional.getJSONObject(0).getString("price"));
                
                inventory = (TextView)findViewById(R.id.inventary);
                inventory.setText(jsonInventary.getJSONObject(0).getString("weight"));
                Global.getInstance().setWeightFromAPI(jsonInventary.getJSONObject(0).getInt("weight_number"));
                
                inventaryDesc = (TextView)findViewById(R.id.inventaryDesc);
                
                dailySales = (TextView)findViewById(R.id.dailySales);
                dailySales.setText(jsonSales.getJSONObject(0).getString("total"));
                Global.getInstance().setSellFromAPI(jsonSales.getJSONObject(0).getInt("total_number"));
                Global.getInstance().setSellIdFromAPI(jsonSales.getJSONObject(0).getInt("sell_id"));

        		dailySales.setTypeface(tf);
        		inventory.setTypeface(tf);
        		inventaryDesc.setTypeface(tf);
                
                Global.getInstance().setSellIdFromAPI(jsonSales.getJSONObject(0).getInt("sell_id"));
                Global.getInstance().setRequesIdFromAPI(jsonRequests.getJSONObject(0).getInt("request_id"));
                
                inventaryLayout = (LinearLayout)findViewById(R.id.inventaryLayout);
                int limit = jsonLimit.getJSONObject(0).getInt("limit");
                
                switch(limit) {
                case 0:
                	inventaryLayout.setBackgroundResource(R.drawable.bg_rojo);
                	break;
                case 1:
                	inventaryLayout.setBackgroundResource(R.drawable.bg_amarillo);
                	inventory.setTextColor(0xffe91c8a);
                	inventaryDesc.setTextColor(0xffe91c8a);
                	break;
                case 2:
                	inventaryLayout.setBackgroundResource(R.drawable.bg_verde);
                	break;
                }

                Boolean current_request = jsonCurrentRequest.getJSONObject(0).getBoolean("status");
                if(current_request == false){
                	currentRequestButton.setAlpha(100);
                	currentRequestButton.setEnabled(true);
                }
                Global.getInstance().setCurrentRequest(jsonCurrentRequest.getJSONObject(0).getString("id"));
                
                Global.getInstance().setAditional(jsonAditional.getJSONObject(0).getInt("price"));
                Global.getInstance().setAditionalBase(jsonAditional.getJSONObject(0).getString("price"));
                
                Global.getInstance().setMin80(jsonCones.getJSONObject(0).getInt("min_weight"));
                Global.getInstance().setMin120(jsonCones.getJSONObject(1).getInt("min_weight"));
                Global.getInstance().setMin200(jsonCones.getJSONObject(2).getInt("min_weight"));
                Global.getInstance().setMax80(jsonCones.getJSONObject(0).getInt("max_weight"));
                Global.getInstance().setMax120(jsonCones.getJSONObject(1).getInt("max_weight"));
                Global.getInstance().setMax200(jsonCones.getJSONObject(2).getInt("max_weight"));
                Global.getInstance().setConePrice80(jsonCones.getJSONObject(0).getInt("cone_price"));
                Global.getInstance().setConePrice120(jsonCones.getJSONObject(1).getInt("cone_price"));
                Global.getInstance().setConePrice200(jsonCones.getJSONObject(2).getInt("cone_price"));
                dbHandler.updateParameters(
                		jsonCones.getJSONObject(0).getInt("price"),
                		jsonCones.getJSONObject(1).getInt("price"),
                		jsonCones.getJSONObject(2).getInt("price"),
                		jsonCones.getJSONObject(0).getString("name"),
                		jsonCones.getJSONObject(1).getString("name"),
                		jsonCones.getJSONObject(2).getString("name"),
                		Global.getInstance().getAditional(),
                		jsonCones.getJSONObject(0).getString("id"),
                		jsonCones.getJSONObject(1).getString("id"),
                		jsonCones.getJSONObject(2).getString("id")
                );
                
                dbHandler.updateTotals(jsonSales.getJSONObject(0).getInt("total_number"), jsonInventary.getJSONObject(0).getInt("weight_number"));
                
            } catch (Exception e) {
            	Toast.makeText(context, "La sesi�n ha expirado: " + e.getMessage(), Toast.LENGTH_LONG).show();
        		closeSessionHelper();
            } finally {
            	super.onPostExecute(json);
            }
	    }
	}
	
	public void closeSession(View view){
		closeSessionHelper();
	}
	
	public void closeSessionHelper(){
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.remove("AuthToken");
        editor.commit();
        Intent intent = new Intent(getApplicationContext(), WelcomeActivity.class);
        startActivity(intent);
        finish();
	}
	
	public void sell80(View view) {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.sell_dialog);

		// Change fonts
		Typeface tf = Typeface.createFromAsset(getAssets(),
                "fonts/HelveticaNeueDeskUI.ttf");
		
		resetAllCurrents();
		
    	units = (EditText)dialog.findViewById(R.id.unitsEdit);
    	price = (TextView)dialog.findViewById(R.id.sellValue);
    	weight = (EditText)dialog.findViewById(R.id.weightEdit);
    	
    	units.setTypeface(tf);
    	price.setTypeface(tf);
    	weight.setTypeface(tf);
		
    	totalSell = Global.getInstance().getTotalSell();
    	totalWeight = Global.getInstance().getTotalWeight();
    	
    	sellUnits = 0;
    	
    	Global.getInstance().setCurrentCone(80);
    	
    	Button large = (Button)dialog.findViewById(R.id.sellConeImage);
    	large.setText(aController.getCones(0).getProductFixedPrice());
    	large.setTypeface(tf);
		
		// Evento para capturar los botones
		Button cancelButton = (Button) dialog.findViewById(R.id.cancel);
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if(totalSell > 0){
					showCancelDialog();
				}
			}
		});

		Button otherButton = (Button) dialog.findViewById(R.id.other);
		otherButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(units.getText().length() != 0 && weight.getText().length() != 0){
					int u = Integer.parseInt(units.getText().toString());
					int w = Integer.parseInt(weight.getText().toString());
					int minWTotal = (u == 1) ? Global.getInstance().getMin80() * u : (Global.getInstance().getMin80() * u) + u;
					int maxWTotal = Global.getInstance().getMax80() * u;
					if(w <= minWTotal){
						String message = "El peso del cono debe ser mayor a " + minWTotal + ". Esta venta ser� cancelada para que se ingrese el cono que corresponde.";
						showAlertDialog(message);
						resetCurrents();
						dialog.dismiss();
						if(totalSell == 0){
							currentSellButton.setVisibility(View.GONE);
						}
					}else if(w >= maxWTotal){
						String message = "El peso del cono debe ser menor a " + maxWTotal + ". Esta venta ser� cancelada para que se ingrese el cono que corresponde.";
						showAlertDialog(message);
						resetCurrents();
						dialog.dismiss();
						if(totalSell == 0){
							currentSellButton.setVisibility(View.GONE);
						}
					}else {
						setGlobals80();
						dialog.dismiss();
					}
				}else
					Toast.makeText(POSActivity.this, "Debe rellenar todos los campos.", Toast.LENGTH_LONG).show();
			}
		});
		
		Button acceptButton = (Button) dialog.findViewById(R.id.accept);
		acceptButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(units.getText().length() != 0 && weight.getText().length() != 0){
					int u = Integer.parseInt(units.getText().toString());
					int w = Integer.parseInt(weight.getText().toString());
					int minWTotal = (u == 1) ? Global.getInstance().getMin80() * u : (Global.getInstance().getMin80() * u) + u;
					int maxWTotal = Global.getInstance().getMax80() * u;
					if(w <= minWTotal){
						String message = "El peso del cono debe ser mayor a " + minWTotal + ". Esta venta ser� cancelada para que se ingrese el cono que corresponde.";
						showAlertDialog(message);
						resetCurrents();
						dialog.dismiss();
						if(totalSell == 0){
							currentSellButton.setVisibility(View.GONE);
						}
					}else if(w >= maxWTotal){
						String message = "El peso del cono debe ser menor a " + maxWTotal + ". Esta venta ser� cancelada para que se ingrese el cono que corresponde.";
						showAlertDialog(message);
						resetCurrents();
						dialog.dismiss();
						if(totalSell == 0){
							currentSellButton.setVisibility(View.GONE);
						}
					}else {
						setGlobals80();
						sellSummaryDialog();
						dialog.dismiss();
					}
				}else
					Toast.makeText(POSActivity.this, "Debe rellenar todos los campos.", Toast.LENGTH_LONG).show();
			}
		});
		
		/*
		 * Captura los backspaces
		 */
		
		units.addTextChangedListener(new TextWatcher() {


			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				int u = 0;
				int curPrice = 0;

				if(!s.toString().isEmpty())
					u = Integer.parseInt(s.toString());
				else
					u = 0;

				Global.getInstance().setCurrentUnits(u);
				curPrice = u * aController.getCones(0).getConePrice();
				price.setText("$" + curPrice);
	    		currentSellButton.setVisibility(View.VISIBLE);
			}
		});

		weight.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				int w = 0;
				int curPrice = 0;

				if(!s.toString().isEmpty())
					w = Integer.parseInt(s.toString());
				else
					w = 0;

				Global.getInstance().setCurrentWeight(w);
				int fixedWeight = 80 * Global.getInstance().getCurrentUnits();
				curPrice = Global.getInstance().getCurrentUnits() * aController.getCones(0).getConePrice();
				
				if(w > fixedWeight){
					int aditional = w - fixedWeight;
					int adPrice = aditional * Global.getInstance().getAditional();
					curPrice += adPrice;
					totalSell += adPrice;
				}
				
	    		currentSellButton.setVisibility(View.VISIBLE);
				price.setText("$" + curPrice);
			}
		});
		
		currentSellButton.setTypeface(tf);
		price.setTypeface(tf);
		
		dialog.show();
	}
	
	public void sell120(View view) {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.sell_dialog);

		// Change fonts
		Typeface tf = Typeface.createFromAsset(getAssets(),
                "fonts/HelveticaNeueDeskUI.ttf");
		
		resetAllCurrents();
		
    	units = (EditText)dialog.findViewById(R.id.unitsEdit);
    	price = (TextView)dialog.findViewById(R.id.sellValue);
    	weight = (EditText)dialog.findViewById(R.id.weightEdit);
    	
    	units.setTypeface(tf);
    	price.setTypeface(tf);
    	weight.setTypeface(tf);
		
    	totalSell = Global.getInstance().getTotalSell();
    	totalWeight = Global.getInstance().getTotalWeight();
    	
    	sellUnits = 0;
    	
    	Global.getInstance().setCurrentCone(120);
    	
    	Button large = (Button)dialog.findViewById(R.id.sellConeImage);
    	large.setText(aController.getCones(1).getProductFixedPrice());
    	large.setTypeface(tf);
		
		Button cancelButton = (Button) dialog.findViewById(R.id.cancel);
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if(totalSell > 0){
					showCancelDialog();
				}
			}
		});

		Button otherButton = (Button) dialog.findViewById(R.id.other);
		otherButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(units.getText().length() != 0 && weight.getText().length() != 0){
					int u = Integer.parseInt(units.getText().toString());
					int w = Integer.parseInt(weight.getText().toString());
					int minWTotal = (u == 1) ? Global.getInstance().getMin120() * u : (Global.getInstance().getMin120() * u) + u;
					int maxWTotal = Global.getInstance().getMax120() * u;
					if(w <= minWTotal){
						String message = "El peso del cono debe ser mayor a " + minWTotal + ". Esta venta ser� cancelada para que se ingrese el cono que corresponde.";
						showAlertDialog(message);
						resetCurrents();
						dialog.dismiss();
						if(totalSell == 0){
							currentSellButton.setVisibility(View.GONE);
						}
					}else if(w >= maxWTotal){
						String message = "El peso del cono debe ser menor a " + maxWTotal + ". Esta venta ser� cancelada para que se ingrese el cono que corresponde.";
						showAlertDialog(message);
						resetCurrents();
						dialog.dismiss();
						if(totalSell == 0){
							currentSellButton.setVisibility(View.GONE);
						}
					}else {
						setGlobals120();
						dialog.dismiss();
					}
				}else
					Toast.makeText(POSActivity.this, "Debe rellenar todos los campos.", Toast.LENGTH_LONG).show();
			}
		});
		
		Button acceptButton = (Button) dialog.findViewById(R.id.accept);
		acceptButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(units.getText().length() != 0 && weight.getText().length() != 0){
					int u = Integer.parseInt(units.getText().toString());
					int w = Integer.parseInt(weight.getText().toString());
					int minWTotal = (u == 1) ? Global.getInstance().getMin120() * u : (Global.getInstance().getMin120() * u) + u;
					int maxWTotal = Global.getInstance().getMax120() * u;
					if(w <= minWTotal){
						String message = "El peso del cono debe ser mayor a " + minWTotal + ". Esta venta ser� cancelada para que se ingrese el cono que corresponde.";
						showAlertDialog(message);
						resetCurrents();
						dialog.dismiss();
						if(totalSell == 0){
							currentSellButton.setVisibility(View.GONE);
						}
					}else if(w >= maxWTotal){
						String message = "El peso del cono debe ser menor a " + maxWTotal + ". Esta venta ser� cancelada para que se ingrese el cono que corresponde.";
						showAlertDialog(message);
						resetCurrents();
						dialog.dismiss();
						if(totalSell == 0){
							currentSellButton.setVisibility(View.GONE);
						}
					}else {
						setGlobals120();
						sellSummaryDialog();
						dialog.dismiss();
					}
				}else
					Toast.makeText(POSActivity.this, "Debe rellenar todos los campos.", Toast.LENGTH_LONG).show();
			}
		});
		
		/*
		 * Captura los backspaces
		 */
		
		units.addTextChangedListener(new TextWatcher() {


			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				int u = 0;
				int curPrice = 0;

				if(!s.toString().isEmpty())
					u = Integer.parseInt(s.toString());
				else
					u = 0;

				Global.getInstance().setCurrentUnits(u);
				curPrice = u * aController.getCones(1).getConePrice();
				price.setText("$" + curPrice);
	    		currentSellButton.setVisibility(View.VISIBLE);
			}
		});

		weight.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				int w = 0;
				int curPrice = 0;

				if(!s.toString().isEmpty())
					w = Integer.parseInt(s.toString());
				else
					w = 0;

				Global.getInstance().setCurrentWeight(w);
				int fixedWeight = 120 * Global.getInstance().getCurrentUnits();
				curPrice = Global.getInstance().getCurrentUnits() * aController.getCones(1).getConePrice();
				
				if(w > fixedWeight){
					int aditional = w - fixedWeight;
					int adPrice = aditional * Global.getInstance().getAditional();
					curPrice += adPrice;
					totalSell += adPrice;
				}
				
	    		currentSellButton.setVisibility(View.VISIBLE);
				price.setText("$" + curPrice);
			}
		});
		
		currentSellButton.setTypeface(tf);
		price.setTypeface(tf);

		dialog.show();
	}
	
	public void sell200(View view) {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.sell_dialog);

		// Change fonts
		Typeface tf = Typeface.createFromAsset(getAssets(),
                "fonts/HelveticaNeueDeskUI.ttf");
		
		resetAllCurrents();
		
    	units = (EditText)dialog.findViewById(R.id.unitsEdit);
    	price = (TextView)dialog.findViewById(R.id.sellValue);
    	weight = (EditText)dialog.findViewById(R.id.weightEdit);
    	
    	units.setTypeface(tf);
    	price.setTypeface(tf);
    	weight.setTypeface(tf);
		
    	totalSell = Global.getInstance().getTotalSell();
    	totalWeight = Global.getInstance().getTotalWeight();
    	
    	sellUnits = 0;
    	
    	Global.getInstance().setCurrentCone(200);
    	
    	Button large = (Button)dialog.findViewById(R.id.sellConeImage);
    	large.setText(aController.getCones(2).getProductFixedPrice());
    	large.setTypeface(tf);
		
		Button cancelButton = (Button) dialog.findViewById(R.id.cancel);
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if(totalSell > 0){
					showCancelDialog();
				}
			}
		});

		Button otherButton = (Button) dialog.findViewById(R.id.other);
		otherButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(units.getText().length() != 0 && weight.getText().length() != 0){
					int u = Integer.parseInt(units.getText().toString());
					int w = Integer.parseInt(weight.getText().toString());
					int minWTotal = (u == 1) ? Global.getInstance().getMin200() * u : (Global.getInstance().getMin200() * u) + u;
					int maxWTotal = Global.getInstance().getMax200() * u;
					if(w <= minWTotal){
						String message = "El peso del cono debe ser mayor a " + minWTotal + ". Esta venta ser� cancelada para que se ingrese el cono que corresponde.";
						showAlertDialog(message);
						resetCurrents();
						dialog.dismiss();
						if(totalSell == 0){
							currentSellButton.setVisibility(View.GONE);
						}
					}else if(w >= maxWTotal){
						String message = "El peso del cono debe ser menor a " + maxWTotal + ". Esta venta ser� cancelada para que se ingrese el cono que corresponde.";
						showAlertDialog(message);
						resetCurrents();
						dialog.dismiss();
						if(totalSell == 0){
							currentSellButton.setVisibility(View.GONE);
						}
					}else {
						setGlobals200();
						dialog.dismiss();
					}
				}else
					Toast.makeText(POSActivity.this, "Debe rellenar todos los campos.", Toast.LENGTH_LONG).show();
			}
		});
		
		Button acceptButton = (Button) dialog.findViewById(R.id.accept);
		acceptButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(units.getText().length() != 0 && weight.getText().length() != 0){
					int u = Integer.parseInt(units.getText().toString());
					int w = Integer.parseInt(weight.getText().toString());
					int minWTotal = (u == 1) ? Global.getInstance().getMin200() * u : (Global.getInstance().getMin200() * u) + u;
					int maxWTotal = Global.getInstance().getMax200() * u;
					if(w <= minWTotal){
						String message = "El peso del cono debe ser mayor a " + minWTotal + ". Esta venta ser� cancelada para que se ingrese el cono que corresponde.";
						showAlertDialog(message);
						resetCurrents();
						dialog.dismiss();
						if(totalSell == 0){
							currentSellButton.setVisibility(View.GONE);
						}
					}else if(w >= maxWTotal){
						String message = "El peso del cono debe ser menor a " + maxWTotal + ". Esta venta ser� cancelada para que se ingrese el cono que corresponde.";
						showAlertDialog(message);
						resetCurrents();
						dialog.dismiss();
						if(totalSell == 0){
							currentSellButton.setVisibility(View.GONE);
						}
					}else {
						setGlobals200();
						sellSummaryDialog();
						dialog.dismiss();
					}
				}else
					Toast.makeText(POSActivity.this, "Debe rellenar todos los campos.", Toast.LENGTH_LONG).show();
			}
		});
		
		/*
		 * Captura los backspaces
		 */
		
		units.addTextChangedListener(new TextWatcher() {


			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				int u = 0;
				int curPrice = 0;

				if(!s.toString().isEmpty())
					u = Integer.parseInt(s.toString());
				else
					u = 0;

				Global.getInstance().setCurrentUnits(u);
				curPrice = u * aController.getCones(2).getConePrice();
				price.setText("$" + curPrice);
	    		currentSellButton.setVisibility(View.VISIBLE);
			}
		});

		weight.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				int w = 0;
				int curPrice = 0;

				if(!s.toString().isEmpty())
					w = Integer.parseInt(s.toString());
				else
					w = 0;

				Global.getInstance().setCurrentWeight(w);
				int fixedWeight = 200 * Global.getInstance().getCurrentUnits();
				curPrice = Global.getInstance().getCurrentUnits() * aController.getCones(2).getConePrice();
				
				if(w > fixedWeight){
					int aditional = w - fixedWeight;
					int adPrice = aditional * Global.getInstance().getAditional();
					curPrice += adPrice;
					totalSell += adPrice;
				}
				
	    		currentSellButton.setVisibility(View.VISIBLE);
				price.setText("$" + curPrice);
			}
		});
		
		currentSellButton.setTypeface(tf);
		price.setTypeface(tf);

		dialog.show();
	}
	
	public void setGlobals80() {
		// Units
		Global.getInstance().setUnits80(Global.getInstance().getCurrentUnits() + Global.getInstance().getUnits80());
		int price = aController.getCones(0).getConePrice() * Global.getInstance().getCurrentUnits();
		
		// Weight
		Global.getInstance().setWeight80(Global.getInstance().getCurrentWeight() + Global.getInstance().getWeight80());
		Global.getInstance().setTotalWeight(Global.getInstance().getCurrentWeight() + Global.getInstance().getTotalWeight());
		int fixed = 80 * Global.getInstance().getCurrentUnits();
		int w = Global.getInstance().getCurrentWeight();
		
		if(w > fixed){
			int add = Global.getInstance().getAditional();
			int wAdd = w - fixed;
			price = (wAdd * add) + price;
		}
		
		// Prices
		Global.getInstance().setSellPrice80(price + Global.getInstance().getSellPrice80());
		Global.getInstance().setTotalSell(price + Global.getInstance().getTotalSell());
	}
	
	public void setGlobals120() {
		// Units
		Global.getInstance().setUnits120(Global.getInstance().getCurrentUnits() + Global.getInstance().getUnits120());
		int price = aController.getCones(1).getConePrice() * Global.getInstance().getCurrentUnits();
		
		// Weight
		Global.getInstance().setWeight120(Global.getInstance().getCurrentWeight() + Global.getInstance().getWeight120());
		Global.getInstance().setTotalWeight(Global.getInstance().getCurrentWeight() + Global.getInstance().getTotalWeight());
		int fixed = 120 * Global.getInstance().getCurrentUnits();
		int w = Global.getInstance().getCurrentWeight();
		
		if(w > fixed){
			int add = Global.getInstance().getAditional();
			int wAdd = w - fixed;
			price = (wAdd * add) + price;
		}
		
		// Prices
		Global.getInstance().setSellPrice120(price + Global.getInstance().getSellPrice120());
		Global.getInstance().setTotalSell(price + Global.getInstance().getTotalSell());
	}
	
	public void setGlobals200() {
		// Units
		Global.getInstance().setUnits200(Global.getInstance().getCurrentUnits() + Global.getInstance().getUnits200());
		int price = aController.getCones(2).getConePrice() * Global.getInstance().getCurrentUnits();
		
		// Weight
		Global.getInstance().setWeight200(Global.getInstance().getCurrentWeight() + Global.getInstance().getWeight200());
		Global.getInstance().setTotalWeight(Global.getInstance().getCurrentWeight() + Global.getInstance().getTotalWeight());
		int fixed = 200 * Global.getInstance().getCurrentUnits();
		int w = Global.getInstance().getCurrentWeight();
		
		if(w > fixed){
			int add = Global.getInstance().getAditional();
			int wAdd = w - fixed;
			price = (wAdd * add) + price;
		}
		
		// Prices
		Global.getInstance().setSellPrice200(price + Global.getInstance().getSellPrice200());
		Global.getInstance().setTotalSell(price + Global.getInstance().getTotalSell());
	}
	
	public void showCancelDialog() {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.cancel_dialog);
		
		// Change fonts
		Typeface tf = Typeface.createFromAsset(getAssets(),
                "fonts/HelveticaNeueDeskUI.ttf");
		
		Button notButton = (Button) dialog.findViewById(R.id.not);
		notButton.setTypeface(tf);
		
		cancelTextDialog = (TextView)dialog.findViewById(R.id.cancelDialogText);
		
		String text = "En este momento hay una venta de: ";
		
		if(Global.getInstance().getUnits80() > 0)
			text += Global.getInstance().getUnits80() + " unidades de 80 gramos ";
		
		if(Global.getInstance().getUnits120() > 0)
			text += Global.getInstance().getUnits120() + " unidades de 120 gramos ";
		
		if(Global.getInstance().getUnits200() > 0)
			text += Global.getInstance().getUnits200() + " unidades de 200 gramos ";
		
		text += "por un total de $" + Global.getInstance().getTotalSell();
		
		cancelTextDialog.setText(text);
		cancelTextDialog.setTypeface(tf);
		
		if(Global.getInstance().getCurrentWeight() == 0 || Global.getInstance().getCurrentUnits() == 0){
			notButton.setVisibility(View.GONE);
		}
		
		Button yesButton = (Button) dialog.findViewById(R.id.yes);
		yesButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				resetGlobals();
	    		currentSellButton.setVisibility(View.GONE);
				dialog.dismiss();
			}
		});
		
		notButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		
		Button onlyThisButton = (Button) dialog.findViewById(R.id.onlyThis);
		onlyThisButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				resetCurrents();
				dialog.dismiss();
			}
		});
		
		dialog.show();
	}
	
	public void resetGlobals() {
		Global.getInstance().setUnits80(0);
		Global.getInstance().setUnits120(0);
		Global.getInstance().setUnits200(0);
		Global.getInstance().setWeight80(0);
		Global.getInstance().setWeight120(0);
		Global.getInstance().setWeight200(0);
		Global.getInstance().setSellPrice80(0);
		Global.getInstance().setSellPrice120(0);
		Global.getInstance().setSellPrice200(0);
		Global.getInstance().setTotalSell(0);
		Global.getInstance().setTotalWeight(0);
		currentSellButton.setVisibility(View.GONE);
	}
	
	public void resetCurrents() {
		Integer units = Global.getInstance().getCurrentUnits();
		Integer weight = Global.getInstance().getCurrentWeight();
		Integer sell = Global.getInstance().getCurrentSell();
		Integer cone = Global.getInstance().getCurrentCone();
		int curUnits = 0;
		
		switch(cone) {
		case 80:
			curUnits = Global.getInstance().getUnits80() - units;
			if (curUnits > 0){
				Global.getInstance().setUnits80(Global.getInstance().getUnits80() - units);
				Global.getInstance().setWeight80(Global.getInstance().getWeight80() - weight);
				Global.getInstance().setSellPrice80(Global.getInstance().getSellPrice80() - sell);
			}else{
				Global.getInstance().setUnits80(0);
				Global.getInstance().setWeight80(0);
				Global.getInstance().setSellPrice80(0);
			}
		    break;
		case 120:
			curUnits = Global.getInstance().getUnits120() - units;
			if (curUnits > 0){
				Global.getInstance().setUnits120(Global.getInstance().getUnits120() - units);
				Global.getInstance().setWeight120(Global.getInstance().getWeight120() - weight);
				Global.getInstance().setSellPrice120(Global.getInstance().getSellPrice120() - sell);
			}else{
				Global.getInstance().setUnits120(0);
				Global.getInstance().setWeight120(0);
				Global.getInstance().setSellPrice120(0);
			}
		    break;
		case 200:
			curUnits = Global.getInstance().getUnits200() - units;
			if (curUnits > 0){
				Global.getInstance().setUnits200(Global.getInstance().getUnits200() - units);
				Global.getInstance().setWeight200(Global.getInstance().getWeight200() - weight);
				Global.getInstance().setSellPrice200(Global.getInstance().getSellPrice200() - sell);
			}else{
				Global.getInstance().setUnits200(0);
				Global.getInstance().setWeight200(0);
				Global.getInstance().setSellPrice200(0);
			}
		    break;
		}
		
		if(curUnits > 0){
			Global.getInstance().setTotalSell(Global.getInstance().getTotalSell() - sell);
			Global.getInstance().setTotalWeight(Global.getInstance().getTotalWeight() - weight);
		}else{
			Global.getInstance().setTotalSell(Global.getInstance().getTotalSell());
			Global.getInstance().setTotalWeight(Global.getInstance().getTotalWeight());
		}
		resetAllCurrents();
	}
	
	public void resetAllCurrents() {
		Global.getInstance().setCurrentCone(0);
		Global.getInstance().setCurrentSell(0);
		Global.getInstance().setCurrentUnits(0);
		Global.getInstance().setCurrentWeight(0);
	}
	
	public void acceptSell() {
		updateTotals();
		
		if(Global.getInstance().hasConnection(POSActivity.this)){
			if(dbHandler.countSellsInt() == 0){
				SellTask sellTask = new SellTask(POSActivity.this);
				sellTask.setMessageLoading("Enviando...");
				sellTask.execute(SELL_URL + "?authentication_token=" + mPreferences.getString("AuthToken", ""));
			}else{
				SellTaskDB sellTaskDB = new SellTaskDB(POSActivity.this);
				sellTaskDB.setMessageLoading("Enviando...");
				sellTaskDB.execute(SELL_URL + "?authentication_token=" + mPreferences.getString("AuthToken", ""));
			}
    	}else{
    		Toast.makeText(POSActivity.this, "El dispositivo no tiene conexi�n a Internet, la venta ser� almacenada en Base de Datos.", Toast.LENGTH_LONG).show();
    		sellOffline();
    	}
	}
	
	private class SellTask extends UrlJsonAsyncTask {
	    public SellTask(Context context) {
	        super(context);
	    }

	    @Override
	    protected JSONObject doInBackground(String... urls) {
	        DefaultHttpClient client = new DefaultHttpClient();
	        HttpPost post = new HttpPost(urls[0]);
	        JSONObject holder = new JSONObject();
	        JSONObject cone80 = new JSONObject();
	        JSONObject cone120 = new JSONObject();
	        JSONObject cone200 = new JSONObject();
	        JSONObject total = new JSONObject();
	        String response = null;
	        JSONObject json = new JSONObject();

	        try {
	            try {
	                json.put("success", false);
	                json.put("info", "El dispositivo no tiene conexi�n a Internet.");
	                // 80
	                if(Global.getInstance().getSellPrice80() > 0){
	                	cone80.put("id", aController.getCones(0).getProductId());
	                	cone80.put("units", Global.getInstance().getUnits80());
	                	cone80.put("weight", Global.getInstance().getWeight80());
	                	cone80.put("sell", Global.getInstance().getSellPrice80());
	                	holder.put("cone80", cone80);
	                }
	                // 120
	                if(Global.getInstance().getSellPrice120() > 0){
	                	cone120.put("id", aController.getCones(1).getProductId());
	                	cone120.put("units", Global.getInstance().getUnits120());
	                	cone120.put("weight", Global.getInstance().getWeight120());
	                	cone120.put("sell", Global.getInstance().getSellPrice120());
	                	holder.put("cone120", cone120);
	                }
	                // 200
	                if(Global.getInstance().getSellPrice200() > 0){
	                	cone200.put("id", aController.getCones(2).getProductId());
	                	cone200.put("units", Global.getInstance().getUnits200());
	                	cone200.put("weight", Global.getInstance().getWeight200());
	                	cone200.put("sell", Global.getInstance().getSellPrice200());
	                	holder.put("cone200", cone200);
	                }
	                // total
	                Global.getInstance().setSellIdFromAPI(Global.getInstance().getSellIdFromAPI() + 1);
	                
	                total.put("sell", Global.getInstance().getTotalSell());
	                total.put("weight", Global.getInstance().getTotalWeight());
	                total.put("sell_id", Global.getInstance().getSellIdFromAPI());
	                holder.put("total", total);
	            	
	                StringEntity se = new StringEntity(holder.toString());
	                post.setEntity(se);
	                
	                // setup the request headers
	                post.setHeader("Accept", "application/json");
	                post.setHeader("Content-Type", "application/json");

	                ResponseHandler<String> responseHandler = new BasicResponseHandler();
	                
	        		if(Global.getInstance().hasConnection(POSActivity.this))
	        			response = client.execute(post, responseHandler);
	        		else
	        			response = null;
	        		
	                json = new JSONObject(response);
	                
	            } catch (HttpResponseException e) {
	                e.printStackTrace();
	                Log.e("ClientProtocol", "" + e);
	                json.put("info", "Email o Contrase�a incorrectos.");
	            } catch (IOException e) {
	                e.printStackTrace();
	                Log.e("IO", "" + e);
	            }
	        } catch (JSONException e) {
	            e.printStackTrace();
	            Log.e("JSON", "" + e);
	        }

	        return json;
	    }

	    @Override
	    protected void onPostExecute(JSONObject json) {
	        try {
	            if (json.getBoolean("success")) {

	            	if(status == 1){
	            		dbHandler.open();
	            		dbHandler.removeSell(Global.getInstance().getSellId());
	            		dbHandler.removeSellItem(Global.getInstance().getSellId());
	            	}
	            	resetGlobals();
	                Intent intent = new Intent(getApplicationContext(), POSActivity.class);
	                startActivity(intent);
	                finish();
	            }
	            Toast.makeText(context, json.getString("info"), Toast.LENGTH_LONG).show();
	        } catch (Exception e) {
	            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
	        } finally {
	            super.onPostExecute(json);
	        }
	    }
	}

	private class SellTaskDB extends UrlJsonAsyncTask {
	    public SellTaskDB(Context context) {
	        super(context);
	    }

	    @Override
	    protected JSONObject doInBackground(String... urls) {
	        DefaultHttpClient client = new DefaultHttpClient();
	        HttpPost post = new HttpPost(urls[0]);
	        JSONObject holder = new JSONObject();
	        JSONObject cone80 = new JSONObject();
	        JSONObject cone120 = new JSONObject();
	        JSONObject cone200 = new JSONObject();
	        JSONObject total = new JSONObject();
	        String response = null;
	        JSONObject json = new JSONObject();

	        int q80 = 0, q120 = 0, q200 = 0;
	        int w80 = 0, w120 = 0, w200 = 0;
	        int v80 = 0, v120 = 0, v200 = 0;
	        String p80 = "", p120 = "", p200 = "";


    		ArrayList<SellItem> sell_items_array = dbHandler.Get_Sell_Items(sellFromBD);

            for (int j = 0; j < sell_items_array.size(); j++) {

                String product = sell_items_array.get(j).getProduct();
                int quantity = sell_items_array.get(j).getQuantity();
                int weight = sell_items_array.get(j).getWeight();
                int value = sell_items_array.get(j).getValue();
                int product_n = sell_items_array.get(j).getProductN();

                if(product_n == 80){
                	q80 = quantity;
                	w80 = weight;
                	v80 = value;
                	p80 = product;
                }
                if(product_n == 120){
                	q120 = quantity;
                	w120 = weight;
                	v120 = value;
                	p120 = product;
                }
                if(product_n == 200){
                	q200 = quantity;
                	w200 = weight;
                	v200 = value;
                	p200 = product;
                }
            }
	        
	        try {
	            try {
	                json.put("success", false);
	                json.put("info", "El dispositivo no tiene conexi�n a Internet.");
	                // 80
	                if(v80 > 0){
	                	cone80.put("id", p80);
	                	cone80.put("units", q80);
	                	cone80.put("weight", w80);
	                	cone80.put("sell", v80);
	                	holder.put("cone80", cone80);
	                }
	                // 120
	                if(v120 > 0){
	                	cone120.put("id", p120);
	                	cone120.put("units", q120);
	                	cone120.put("weight", w120);
	                	cone120.put("sell", v120);
	                	holder.put("cone120", cone120);
	                }
	                // 200
	                if(v200 > 0){
	                	cone200.put("id", p200);
	                	cone200.put("units", q200);
	                	cone200.put("weight", w200);
	                	cone200.put("sell", v200);
	                	holder.put("cone200", cone200);
	                }
	                // total
	                
	                response = null;
	                
	                if(v80 == 0 && v120 == 0 && v200 == 0){
		                json.put("success", false);
		                json.put("info", "No hay datos suficientes para hacer la venta.");
	                }else{
		                total.put("sell", Global.getInstance().getTotalSell());
		                total.put("weight", Global.getInstance().getTotalWeight());
		                total.put("sell_id", sellFromBD);
		                holder.put("total", total);
		            	
		                StringEntity se = new StringEntity(holder.toString());
		                post.setEntity(se);
		                
		                // setup the request headers
		                post.setHeader("Accept", "application/json");
		                post.setHeader("Content-Type", "application/json");

		                ResponseHandler<String> responseHandler = new BasicResponseHandler();
		                response = client.execute(post, responseHandler);
	                }	
	        		
	                json = new JSONObject(response);
	                
	            } catch (HttpResponseException e) {
	                e.printStackTrace();
	                json.put("info", "Algo pas�: " + e);
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        } catch (JSONException e) {
	            e.printStackTrace();
	        }

	        return json;
	    }

	    @Override
	    protected void onPostExecute(JSONObject json) {
	        try {
	            if (json.getBoolean("success")) {

	            	dbHandler.open();
	            	dbHandler.removeSell(sellFromBD);
	            	dbHandler.removeSellItem(sellFromBD);

	            	resetGlobals();
	                Intent intent = new Intent(getApplicationContext(), POSActivity.class);
	                startActivity(intent);
	                finish();
	            }
	            Toast.makeText(context, json.getString("info"), Toast.LENGTH_LONG).show();
	        } catch (Exception e) {
	            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
	        } finally {
	            super.onPostExecute(json);
	        }
	    }
	}

	public void showCurrentSell(View view) {
		sellSummaryDialog();
	}
	
	public void sellSummaryDialog() {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.summary_dialog);

		// Change fonts
		Typeface tf = Typeface.createFromAsset(getAssets(),
                "fonts/HelveticaNeueDeskUI.ttf");
		
		TableLayout summaryLayout = (TableLayout) dialog.findViewById(R.id.summaryTable);

        // 80
        if(Global.getInstance().getSellPrice80() > 0){
    		TableRow row = (TableRow) getLayoutInflater().inflate(R.layout.summary_row, null);
    		((TextView)row.findViewById(R.id.unitsSummary)).setText(Global.getInstance().getUnits80()+"");
    		((TextView)row.findViewById(R.id.unitsSummary)).setTypeface(tf);
    		((TextView)row.findViewById(R.id.weightSummary)).setText(Global.getInstance().getWeight80()+" (g)");
    		((TextView)row.findViewById(R.id.weightSummary)).setTypeface(tf);
    		((TextView)row.findViewById(R.id.valueSummary)).setText("$ "+Global.getInstance().getSellPrice80());
    		((TextView)row.findViewById(R.id.valueSummary)).setTypeface(tf);
    		Button thumb = (Button)row.findViewById(R.id.imageSummary);
    		thumb.setText(aController.getCones(0).getProductFixedPrice());
    		thumb.setTypeface(tf);
            
    		summaryLayout.addView(row);
    		summaryLayout.requestLayout();
        }
        // 120
        if(Global.getInstance().getSellPrice120() > 0){
    		TableRow row = (TableRow) getLayoutInflater().inflate(R.layout.summary_row, null);
    		((TextView)row.findViewById(R.id.unitsSummary)).setText(Global.getInstance().getUnits120()+"");
    		((TextView)row.findViewById(R.id.unitsSummary)).setTypeface(tf);
    		((TextView)row.findViewById(R.id.weightSummary)).setText(Global.getInstance().getWeight120()+" (g)");
    		((TextView)row.findViewById(R.id.weightSummary)).setTypeface(tf);
    		((TextView)row.findViewById(R.id.valueSummary)).setText("$ "+Global.getInstance().getSellPrice120());
    		((TextView)row.findViewById(R.id.valueSummary)).setTypeface(tf);
    		Button thumb = (Button)row.findViewById(R.id.imageSummary);
    		thumb.setText(aController.getCones(1).getProductFixedPrice());
    		thumb.setTypeface(tf);
    		
    		summaryLayout.addView(row);
    		summaryLayout.requestLayout();
        }
        // 200
        if(Global.getInstance().getSellPrice200() > 0){
    		TableRow row = (TableRow) getLayoutInflater().inflate(R.layout.summary_row, null);
    		((TextView)row.findViewById(R.id.unitsSummary)).setText(Global.getInstance().getUnits200()+"");
    		((TextView)row.findViewById(R.id.unitsSummary)).setTypeface(tf);
    		((TextView)row.findViewById(R.id.weightSummary)).setText(Global.getInstance().getWeight200()+" (g)");
    		((TextView)row.findViewById(R.id.weightSummary)).setTypeface(tf);
    		((TextView)row.findViewById(R.id.valueSummary)).setText("$ "+Global.getInstance().getSellPrice200());
    		((TextView)row.findViewById(R.id.valueSummary)).setTypeface(tf);
    		Button thumb = (Button)row.findViewById(R.id.imageSummary);
    		thumb.setText(aController.getCones(2).getProductFixedPrice());
    		thumb.setTypeface(tf);
    		
    		summaryLayout.addView(row);
    		summaryLayout.requestLayout();
        }
        
        TableRow rowTotal = (TableRow) getLayoutInflater().inflate(R.layout.total_summary_row, null);
        ((TextView)rowTotal.findViewById(R.id.totalSummary)).setText("$ "+Global.getInstance().getTotalSell());
        ((TextView)rowTotal.findViewById(R.id.totalSummary)).setTypeface(tf);
        summaryLayout.addView(rowTotal);
		summaryLayout.requestLayout();
		
		Button acceptButton = (Button) dialog.findViewById(R.id.accept);
		acceptButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				acceptSell();
				dialog.dismiss();
			}
		});
		
		Button cancelButton = (Button) dialog.findViewById(R.id.cancel);
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				resetGlobals();
				dialog.dismiss();
			}
		});
		
		dialog.show();
	}
	
	public void sellOffline() {
		dbHandler.open();
		Integer sell_id = 1;
		
        Cursor params = dbHandler.getParameters();
		if (params != null){
			params.moveToFirst();
			if(params.getInt (0) != 0){

				dbHandler.insertSell(Global.getInstance().getTotalSell(), Global.getInstance().getTotalWeight());
				
				sell_id = dbHandler.getLastSellId();

				id80 = params.getString(8);
				id120 = params.getString(9);
				id200 = params.getString(10);
			}else{
				notEnoughData();
			}
		}
		
        // 80
        if(Global.getInstance().getSellPrice80() > 0){
        	dbHandler.insertSellItem(id80,
        			Global.getInstance().getUnits80(), 
        			Global.getInstance().getWeight80(), 
        			Global.getInstance().getSellPrice80(), 
        			sell_id, 80);
        }
        // 120
        if(Global.getInstance().getSellPrice120() > 0){
        	dbHandler.insertSellItem(id120,
        			Global.getInstance().getUnits120(), 
        			Global.getInstance().getWeight120(), 
        			Global.getInstance().getSellPrice120(), 
        			sell_id, 120);
        }
        // 200
        if(Global.getInstance().getSellPrice200() > 0){
        	dbHandler.insertSellItem(id200,
        			Global.getInstance().getUnits200(), 
        			Global.getInstance().getWeight200(), 
        			Global.getInstance().getSellPrice200(), 
        			sell_id, 200);
        }
        
        dbHandler.close();
    	resetGlobals();
        Intent intent = new Intent(getApplicationContext(), POSActivity.class);
        startActivity(intent);
        finish();
	}
	
	public void sendSells() {
		
		ArrayList<Sell> sells_array = dbHandler.Get_Sells();

        for (int i = 0; i < sells_array.size(); i++) {
            int tidno = sells_array.get(i).getID();
            int total_sell = sells_array.get(i).getTotalSell();
            int total_weight = sells_array.get(i).getTotalWeight();
            
            Global.getInstance().setTotalSell(total_sell);
            Global.getInstance().setTotalWeight(total_weight);
            
            sellFromBD = tidno;
            acceptSell();
        }
        dbHandler.close();
	}
	
	public void setConesValuesOffLine() {
		price1 = (TextView)findViewById(R.id.price1);
		cone1 = (TextView)findViewById(R.id.cone1);
		price2 = (TextView)findViewById(R.id.price2);
		cone2 = (TextView)findViewById(R.id.cone2);
		price3 = (TextView)findViewById(R.id.price3);
		cone3 = (TextView)findViewById(R.id.cone3);
        aditional = (TextView)findViewById(R.id.aditional);
        
        TableLayout conesLayout = (TableLayout) findViewById(R.id.conesTable);
        
        for(int n = 1, s = conesLayout.getChildCount(); n < s; ++n) {
        	TableRow row = (TableRow)conesLayout.getChildAt(n);
        	row.setVisibility(View.GONE);
        }
        
    	TableRow namesRow = (TableRow) getLayoutInflater().inflate(R.layout.cone_names_row, null);
    	TableRow imagesRow = (TableRow) getLayoutInflater().inflate(R.layout.cone_buttons_row, null);
    	TableRow pricesRow = (TableRow) getLayoutInflater().inflate(R.layout.cone_prices_row, null);
		
        Cursor cur = dbHandler.hasParameters();
        if(cur == null){
        	notEnoughData();
        }else{
        	cur.moveToFirst();
        	if(cur.getInt(0) == 0){
        		notEnoughData();
        	}else{
                Cursor params = dbHandler.getParameters();
        		if (params != null){
        			params.moveToFirst();
        			if(params.getInt (0) != 0){
        				int value80 = params.getInt(1);
        				int value120 = params.getInt(2);
        				int value200 = params.getInt(3);
        				String name80 = params.getString(4);
        				String name120 = params.getString(5);
        				String name200 = params.getString(6);
        				int ad = params.getInt(7);
        				String id80 = params.getString(8);
        				String id120 = params.getString(9);
        				String id200 = params.getString(10);

        				aditional.setText("$"+ad);
        				
                    	// Names
                    	TextView tvName1 = (TextView)namesRow.getChildAt(0);
                    	tvName1.setText(name80);
                    	TextView tvName2 = (TextView)namesRow.getChildAt(1);
                    	tvName2.setText(name120);
                    	TextView tvName3 = (TextView)namesRow.getChildAt(2);
                    	tvName3.setText(name200);
                    	
                    	// Prices
                    	TextView tvPrice1 = (TextView)pricesRow.getChildAt(0);
                    	tvPrice1.setText("$"+value80);
                    	TextView tvPrice2 = (TextView)pricesRow.getChildAt(1);
                    	tvPrice2.setText("$"+value120);
                    	TextView tvPrice3 = (TextView)pricesRow.getChildAt(2);
                    	tvPrice3.setText("$"+value200);

                    	coneObject80 = new ModelCones(id80, name80, null, "$"+value80, value80, Global.getInstance().getMin80(), Global.getInstance().getConePrice80(), Global.getInstance().getMax80());
                    	coneObject120 = new ModelCones(id120, name120, null, "$"+value120, value120, Global.getInstance().getMin120(), Global.getInstance().getConePrice120(), Global.getInstance().getMax120());
                    	coneObject200 = new ModelCones(id200, name200, null, "$"+value200, value200, Global.getInstance().getMin200(), Global.getInstance().getConePrice200(), Global.getInstance().getMax200());

                        aController.setCones(coneObject80);
                        aController.setCones(coneObject120);
                        aController.setCones(coneObject200);
                        
        				Global.getInstance().setAditional(ad);
                		
                		conesLayout.addView(namesRow);
                		conesLayout.addView(imagesRow);
                		conesLayout.addView(pricesRow);
                		conesLayout.requestLayout();
        				
        			} else {
        				price1.setText(aController.getCones(0).getProductFixedPrice());
        				cone1.setText(aController.getCones(0).getProductName());
        				price2.setText(aController.getCones(1).getProductFixedPrice());
        				cone2.setText(aController.getCones(1).getProductName());
        				price3.setText(aController.getCones(2).getProductFixedPrice());
        				cone3.setText(aController.getCones(2).getProductName());
        		        aditional.setText(Global.getInstance().getAditionalBase());
        			}
        		} else {
    				price1.setText(aController.getCones(0).getProductFixedPrice());
    				cone1.setText(aController.getCones(0).getProductName());
    				price2.setText(aController.getCones(1).getProductFixedPrice());
    				cone2.setText(aController.getCones(1).getProductName());
    				price3.setText(aController.getCones(2).getProductFixedPrice());
    				cone3.setText(aController.getCones(2).getProductName());
    		        aditional.setText(Global.getInstance().getAditionalBase());
        		}
                
        		inventory = (TextView)findViewById(R.id.inventary);
        		dailySales = (TextView)findViewById(R.id.dailySales);
        		
                Cursor totals = dbHandler.getTotals();
        		if (totals != null){
        			totals.moveToFirst();
        			if(totals.getInt (0) != 0){
        				int sell = totals.getInt(1);
        				int weight = totals.getInt(2);
        				
        				float f = (float) (Math.round(weight*1.0f)/1000.0f);
        				
        				inventory.setText(f + " Kg");
        				dailySales.setText("$"+sell);
        			} else {
        				inventory.setText(Global.getInstance().getWeightFromAPI());
        				dailySales.setText(Global.getInstance().getSellFromAPI());
        			}
        		} else {
        			inventory.setText(Global.getInstance().getWeightFromAPI());
        			dailySales.setText(Global.getInstance().getSellFromAPI());
        		}
        	}
        }
        
	}
	
	public void updateTotals() {
		int sell = 0, weight = 0;
		if(Global.getInstance().getSellFromAPI() == 0 || Global.getInstance().getTotalWeight() == 0){
			sell = 0;
			weight = 0;
		}else{
			sell = Global.getInstance().getTotalSell() + Global.getInstance().getSellFromAPI();
	        weight = Global.getInstance().getWeightFromAPI() - Global.getInstance().getTotalWeight();
		}
		
        Global.getInstance().setSellFromAPI(sell);
        Global.getInstance().setWeightFromAPI(weight);
        
        dbHandler.updateTotals(sell, weight);
	}
	
	public void showAlertDialog(String message) {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alert_dialog);
		
		// Change fonts
		Typeface tf = Typeface.createFromAsset(getAssets(),
                "fonts/HelveticaNeueDeskUI.ttf");
		
		Button acceptAlert = (Button) dialog.findViewById(R.id.acceptAlert);
		acceptAlert.setTypeface(tf);
		
		alertTextDialog = (TextView)dialog.findViewById(R.id.alertDialogText);
		
		alertTextDialog.setText(message);
		alertTextDialog.setTypeface(tf);
		
		acceptAlert.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				resetCurrents();
				dialog.dismiss();
			}
		});
		
		dialog.show();
	}
	
	public void notEnoughData() {
		Toast.makeText(POSActivity.this, "Es necesario que el dispositivo tenga conexi�n a Internet para hacer la carga inicial de datos.", Toast.LENGTH_LONG).show();
		resetGlobals();
        Intent intent = new Intent(getApplicationContext(), WelcomeActivity.class);
        startActivity(intent);
        finish();
	}

	public void historicalSales(View view) {
        Intent intent = new Intent(getApplicationContext(), HistoricalSales.class);
        startActivity(intent);
        finish();
	}

	public void requests(View view) {
        Intent intent = new Intent(getApplicationContext(), RequestActivity.class);
        startActivity(intent);
        finish();
	}
	
	public void showCurrentRequest(View view) {
        Intent intent = new Intent(getApplicationContext(), AcceptRequestActivity.class);
        startActivity(intent);
        finish();
	}

}