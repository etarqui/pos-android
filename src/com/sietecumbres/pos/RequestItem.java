package com.sietecumbres.pos;

public class RequestItem {

	// private variables
	public int _id;
	public String _product;
	public int _quantity;
	public int _value;
	public int _request_id;

	public RequestItem() {
	}

	// constructor
	public RequestItem(int id, String product, int quantity, int value, int request_id) {
		this._id = id;
		this._product = product;
		this._quantity = quantity;
		this._value = value;
		this._request_id = request_id;

	}

	// constructor
	public RequestItem(String product, int quantity, int value, int request_id) {
		this._product = product;
		this._quantity = quantity;
		this._value = value;
		this._request_id = request_id;
	}

	// getting ID
	public int getID() {
		return this._id;
	}

	// setting id
	public void setID(int id) {
		this._id = id;
	}

	// getting name
	public String getProduct() {
		return this._product;
	}

	// setting name
	public void setProduct(String product) {
		this._product = product;
	}

	// getting name
	public int getQuantity() {
		return this._quantity;
	}

	// setting name
	public void setQuantity(int quantity) {
		this._quantity = quantity;
	}

	// getting phone number
	public int getValue() {
		return this._value;
	}

	// setting phone number
	public void setValue(int value) {
		this._value = value;
	}

	// getting phone number
	public int getRequestId() {
		return this._request_id;
	}

	// setting phone number
	public void setRequestId(int request_id) {
		this._request_id = request_id;
	}

}