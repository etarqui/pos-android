package com.sietecumbres.pos;

public class ModelUnitsAccept {

	private int productUnits;

	public ModelUnitsAccept(int productUnits){
		this.productUnits = productUnits;
	}

	public int getProductUnits() {
		return productUnits;
	}

	public void setProductUnits(int Units) {
		this.productUnits = Units;
	}
}
