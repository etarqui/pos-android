package com.sietecumbres.pos;

import java.io.IOException;

import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.app.SherlockActivity;
import com.savagelook.android.UrlJsonAsyncTask;

import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WelcomeActivity extends SherlockActivity {
	
	private final static String LOGIN_API_ENDPOINT_URL = Global.getInstance().getDevelopment() + "/api/v1/sessions.json";
	private SharedPreferences mPreferences;
	private String mUserEmail;
	private String mUserPassword;
	
	EditText userEmailField, userPasswordField;
	Button loginButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);
		
		mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/HelveticaNeueDeskUI.ttf");
	    userEmailField = (EditText) findViewById(R.id.userEmail);
	    userEmailField.setTypeface(tf);
	    userPasswordField = (EditText) findViewById(R.id.userPassword);
	    userPasswordField.setTypeface(tf);
	    loginButton = (Button) findViewById(R.id.loginButton);
	    loginButton.setTypeface(tf);
	}
	
	@Override
	public void onBackPressed() {
	    Intent startMain = new Intent(Intent.ACTION_MAIN);
	    startMain.addCategory(Intent.CATEGORY_HOME);
	    startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    startActivity(startMain);
	    finish();
	}
	
	public void login(View button) {

	    mUserEmail = userEmailField.getText().toString();
	    mUserPassword = userPasswordField.getText().toString();

	    if (mUserEmail.length() == 0 || mUserPassword.length() == 0) {
	        // input fields are empty
	        Toast.makeText(this, "Por favor complete los campos.",
	            Toast.LENGTH_LONG).show();
	        return;
	    } else {
	    	if(Global.getInstance().hasConnection(WelcomeActivity.this)){
	    		LoginTask loginTask = new LoginTask(WelcomeActivity.this);
	    		loginTask.setMessageLoading("Iniciando sesi�n...");
	    		loginTask.execute(LOGIN_API_ENDPOINT_URL);
	    	}else{
	    		Toast.makeText(WelcomeActivity.this, "El dispositivo no tiene conexi�n a Internet.", Toast.LENGTH_LONG).show();
	    	}
	    }
	}
	
	private class LoginTask extends UrlJsonAsyncTask {
	    public LoginTask(Context context) {
	        super(context);
	    }

	    @Override
	    protected JSONObject doInBackground(String... urls) {
	        DefaultHttpClient client = new DefaultHttpClient();
	        HttpPost post = new HttpPost(urls[0]);
	        JSONObject holder = new JSONObject();
	        JSONObject userObj = new JSONObject();
	        String response = null;
	        JSONObject json = new JSONObject();

	        try {
	            try {
	                // setup the returned values in case
	                // something goes wrong
	                json.put("success", false);
	                json.put("info", "No tiene conexi�n a internet, por favor revise el dispositivo.");
	                // add the user email and password to
	                // the params
	                userObj.put("email", mUserEmail);
	                userObj.put("password", mUserPassword);
	                holder.put("user", userObj);
	                StringEntity se = new StringEntity(holder.toString());
	                post.setEntity(se);
	                
	                // setup the request headers
	                post.setHeader("Accept", "application/json");
	                post.setHeader("Content-Type", "application/json");

	                ResponseHandler<String> responseHandler = new BasicResponseHandler();
	                response = client.execute(post, responseHandler);
	                json = new JSONObject(response);

	            } catch (HttpResponseException e) {
	                e.printStackTrace();
	                Log.e("ClientProtocol", "" + e);
	                json.put("info", "Email o Contrase�a incorrectos.");
	            } catch (IOException e) {
	                e.printStackTrace();
	                Log.e("IO", "" + e);
	            }
	        } catch (JSONException e) {
	            e.printStackTrace();
	            Log.e("JSON", "" + e);
	        }

	        return json;
	    }

	    @Override
	    protected void onPostExecute(JSONObject json) {
	        try {
	            if (json.getBoolean("success")) {
	                // everything is ok
	                SharedPreferences.Editor editor = mPreferences.edit();
	                // save the returned auth_token into
	                // the SharedPreferences
	                editor.putString("AuthToken", json.getJSONObject("data").getString("auth_token"));
	                editor.putString("Role", json.getJSONObject("data").getString("role"));
	                Global.getInstance().setRole(json.getJSONObject("data").getString("role"));
	                editor.commit();

	                // launch the HomeActivity and close this one
	                Intent intent = new Intent(getApplicationContext(), POSActivity.class);
	                startActivity(intent);
	                finish();
	            }
	            Toast.makeText(context, json.getString("info"), Toast.LENGTH_LONG).show();
	        } catch (Exception e) {
	            // something went wrong: show a Toast
	            // with the exception message
	            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
	        } finally {
	            super.onPostExecute(json);
	        }
	    }
	}

}
